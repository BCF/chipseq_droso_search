# chip-seq_droso_search

**NOTE: ChIP-seq droso search has been migrated to https://gitlab.com/habermannlab/chipseq_droso_search and is discontinued on framagit**

Program retrieving some particular chip-seq experiment data of Drosophilla from NCBI GEO db.
